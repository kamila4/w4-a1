package com.demo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FormServlet extends HttpServlet {
	
	public void service(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		String name=req.getParameter("txtname");
		String pwd=req.getParameter("txtpwd");
		String email=req.getParameter("txtemail");
		String gen=req.getParameter("gender");
		String course=req.getParameter("course");
		String agree=req.getParameter("agreement");
		
		PrintWriter out = res.getWriter();
		if(agree==null)
		{
			out.println("Please accept terms and conditions");
		}
		else {
			out.println("Welcome "+name);
			out.println("DETAILS:");
			out.println("Email "+email);
			out.println("Gender "+gen);
			out.println("Course "+course);

		}
		
		
		
	}

}